﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TechWiz_Roes_Team.Startup))]
namespace TechWiz_Roes_Team
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
