﻿using System.Web;
using System.Web.Mvc;

namespace TechWiz_Roes_Team
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
