namespace TechWiz_Roes_Team.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class feedback : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Feedbacks", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Feedbacks", "Content", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Feedbacks", "Content", c => c.String());
            AlterColumn("dbo.Feedbacks", "Title", c => c.String());
        }
    }
}
