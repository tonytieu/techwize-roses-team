namespace TechWiz_Roes_Team.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class product1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Products", "Subdescription", c => c.String(nullable: false));
            AlterColumn("dbo.Products", "Content", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Products", "Content", c => c.String());
            AlterColumn("dbo.Products", "Subdescription", c => c.String());
        }
    }
}
