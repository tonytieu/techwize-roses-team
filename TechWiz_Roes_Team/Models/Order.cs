﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechWiz_Roes_Team.Models
{
    public class Order : BaseEntity
    {
        public OrderStatus Status { get; set; }
        public string Note { get; set; }
        public string Address { get; set; }
        public double Total { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }

    public class OrderDetail : BaseEntity
    {
        public double Price { get; set; }
        public int Quantity { get; set; }

        public Guid OrderId { get; set; }

        public Guid ProductId { get; set; }
        public Product Product { get; set; }
    }

    public enum OrderStatus
    {
        Draft, Submitted, Confirmed, Delivered, Cancelled
    }
}