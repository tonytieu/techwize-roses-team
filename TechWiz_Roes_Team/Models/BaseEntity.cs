﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TechWiz_Roes_Team.Models
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
        public DateTime CreatedOn { get; set; }

        public BaseEntity()
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.Now;
        }
    }
}