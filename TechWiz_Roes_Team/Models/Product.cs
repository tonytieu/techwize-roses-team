﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Web;

namespace TechWiz_Roes_Team.Models
{
    public class Product : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public Category Category { get; set; }
        [Required]
        public string Subdescription { get; set; }
        [Required]
        public string Content { get; set; }
        public double Price { get; set; }
        public string Avatar { get; set; }
        [NotMapped]
        public string[] Images
        {
            get
            {
                if (!string.IsNullOrEmpty(ImagesJson))
                {
                    var ser = new Newtonsoft.Json.JsonSerializer();
                    var jr = new JsonTextReader(new StringReader(ImagesJson));

                    return ser.Deserialize<string[]>(jr);
                }

                return new string[0];
            }
            set
            {
                var ser = new Newtonsoft.Json.JsonSerializer();
                var sw = new StringWriter();
                ser.Serialize(sw, value);
                ImagesJson = sw.ToString();
            }
        }
        public string ImagesJson { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }

    public enum Category
    {
        categori1,
        category2,
        categori3
    }
}